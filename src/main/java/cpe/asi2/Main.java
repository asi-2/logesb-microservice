package cpe.asi2;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;



@EnableJms
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "LogEsb Microservice", version = "1.0"))
// doc here localhost:8080/swagger-ui.html
public class Main {


    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);

    }
}

