package cpe.asi2.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cpe.asi2.model.EsbMessageModel;
import cpe.asi2.service.EsbReceiverService;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Session;


@Component
public class EsbReceiverController {
    private final EsbReceiverService esbReceiverService;

    public EsbReceiverController(EsbReceiverService esbReceiverService) {
        this.esbReceiverService = esbReceiverService;
    }


    @JmsListener(destination = "*")
    public void receiveMessage(Message<String> message,  Session session) throws JMSException {
            this.esbReceiverService.handleEsbMessage(message);

     //   session.commit();
    }

    public static <T> EsbMessageModel<T> parseMessage(String json) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            TypeReference<EsbMessageModel<T>> typeReference = new TypeReference<EsbMessageModel<T>>() {
            };
            return objectMapper.readValue(json, typeReference);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
