package cpe.asi2.service;

import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

@Service
public class LogEsbService {
    public LogEsbService() {
    }


    public Boolean saveToFile(String queueName,String messageEsb) {
        File file;

        try (FileWriter fileWriter = new FileWriter("log.txt", true)) {
            fileWriter.write("\n*************************************************\n");
            fileWriter.write("Destination: "+ queueName+ "\n");
            fileWriter.write(messageEsb+"\n");

            System.out.println("Data has been written to the file.");
            return Boolean.TRUE;
        } catch (IOException e) {
            e.printStackTrace();
        }



        return Boolean.FALSE;
    }
}
