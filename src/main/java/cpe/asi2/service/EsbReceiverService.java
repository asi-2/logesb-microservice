package cpe.asi2.service;

import cpe.asi2.model.EsbMessageModel;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import javax.jms.Destination;
import java.util.Optional;

@Service
public class EsbReceiverService {
    private final LogEsbService logEsbService;
    public EsbReceiverService(LogEsbService logEsbService) {
        this.logEsbService = logEsbService;
    }
    private String extractQueueName( Destination destination) {
        if(destination == null){
            return "";
        }
        if (destination.toString().startsWith("queue://")) {
            return destination.toString().substring("queue://".length());
        }

        return destination.toString();
    }
    public void handleEsbMessage(  Message<String> message){
        Destination destination = message.getHeaders().get("jms_destination", Destination.class);
        System.out.println("message");
            String destinationName = extractQueueName(destination);

        System.out.println(destination.toString());
        String jsonMessage = message.getPayload();
        System.out.println("--> " + jsonMessage);
        this.logEsbService.saveToFile(destinationName,jsonMessage);
    }
}
